# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Just Perfection
# This file is distributed under the same license as the Just Perfection package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Just Perfection 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-22 11:33-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ui/prefs.ui:73
msgid "Support"
msgstr ""

#: ui/prefs.ui:87
msgid ""
"Your support helps the development process continue and brings more features "
"to the extension"
msgstr ""

#: ui/prefs.ui:298
msgid ""
"You can support my work via Patreon. Also your name will be mentioned at the "
"end of videos on my YouTube Channel as Patreon supporter."
msgstr ""

#: ui/prefs.ui:457
msgid "No Results Found"
msgstr ""

#: ui/prefs.ui:479
msgid "Override"
msgstr ""

#: ui/prefs.ui:536
msgid "Shell Theme"
msgstr ""

#: ui/prefs.ui:547
msgid "Overrides the shell theme partially to create a minimal desktop"
msgstr ""

#: ui/prefs.ui:597
msgid "Visibility"
msgstr ""

#: ui/prefs.ui:646
msgid "Panel"
msgstr ""

#: ui/prefs.ui:695
msgid "Activities Button"
msgstr ""

#: ui/prefs.ui:744
msgid "App Menu"
msgstr ""

#: ui/prefs.ui:793
msgid "Clock Menu"
msgstr ""

#: ui/prefs.ui:842
msgid "Keyboard Layout"
msgstr ""

#: ui/prefs.ui:891
msgid "Accessibility Menu"
msgstr ""

#: ui/prefs.ui:940
msgid "System Menu (Aggregate Menu)"
msgstr ""

#: ui/prefs.ui:989
msgid "Search"
msgstr ""

#: ui/prefs.ui:1038
msgid "Dash"
msgstr ""

#: ui/prefs.ui:1087
msgid "Show Applications Button"
msgstr ""

#: ui/prefs.ui:1136
msgid "On Screen Display (OSD)"
msgstr ""

#: ui/prefs.ui:1185
msgid "Workspace Popup"
msgstr ""

#: ui/prefs.ui:1234
msgid "Workspace Switcher"
msgstr ""

#: ui/prefs.ui:1283
msgid "Background Menu"
msgstr ""

#: ui/prefs.ui:1324
msgid "Icons"
msgstr ""

#: ui/prefs.ui:1373
msgid "App Menu Icon"
msgstr ""

#: ui/prefs.ui:1422
msgid "Panel Notification Icon"
msgstr ""

#: ui/prefs.ui:1471
msgid "Power Icon"
msgstr ""

#: ui/prefs.ui:1520
msgid "Panel Arrow"
msgstr ""

#: ui/prefs.ui:1569
msgid "Window Picker Icon"
msgstr ""

#: ui/prefs.ui:1610
msgid "Behavior"
msgstr ""

#: ui/prefs.ui:1659
msgid "Hot Corner"
msgstr ""

#: ui/prefs.ui:1708
msgid "App Gesture"
msgstr ""

#: ui/prefs.ui:1763
msgid "Type to Search"
msgstr ""

#: ui/prefs.ui:1774
msgid ""
"You can start search without search entry or even focusing on it in overview"
msgstr ""

#: ui/prefs.ui:1824
msgid "Customize"
msgstr ""

#: ui/prefs.ui:1872
msgid "Panel Corner Size"
msgstr ""

#: ui/prefs.ui:1883
msgid "By Shell Theme"
msgstr ""

#: ui/prefs.ui:1884
msgid "No Corner"
msgstr ""

#: ui/prefs.ui:1984
msgid "Panel Position"
msgstr ""

#: ui/prefs.ui:1995
msgid "Top"
msgstr ""

#: ui/prefs.ui:1996
msgid "Bottom"
msgstr ""

#: ui/prefs.ui:2036
msgid "Clock Menu Position"
msgstr ""

#: ui/prefs.ui:2047
msgid "Center"
msgstr ""

#: ui/prefs.ui:2048
msgid "Right"
msgstr ""

#: ui/prefs.ui:2049
msgid "Left"
msgstr ""

#: ui/prefs.ui:2089
msgid "Clock Menu Position Offset"
msgstr ""

#: ui/prefs.ui:2149
msgid "Workspace Switcher Size"
msgstr ""

#: ui/prefs.ui:2160
msgid "Default"
msgstr ""

#: ui/prefs.ui:2210
msgid "Animation"
msgstr ""

#: ui/prefs.ui:2221
msgid "No Animation"
msgstr ""

#: ui/prefs.ui:2222
msgid "Default Speed"
msgstr ""

#: ui/prefs.ui:2223
msgid "Fastest"
msgstr ""

#: ui/prefs.ui:2224
msgid "Faster"
msgstr ""

#: ui/prefs.ui:2225
msgid "Fast"
msgstr ""

#: ui/prefs.ui:2226
msgid "Slow"
msgstr ""

#: ui/prefs.ui:2227
msgid "Slower"
msgstr ""

#: ui/prefs.ui:2228
msgid "Slowest"
msgstr ""
